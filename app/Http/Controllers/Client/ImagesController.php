<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Image;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $images = Image::orderBy('id', 'desc')->paginate(6);

        return view('client.images.index', compact('images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('client.images.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $image = new Image();

        $request->validate([
            'user_id' => ['required'],
            'img' => ['required', 'mimes:jpg,png'],
            'name' => ['required', 'max:192', 'min:2', 'string'],
        ]);

        $image->user_id = $request->get('user_id');
        $image->name = $request->get('name');
        if ($request->hasFile('img')) {
            $image->img = $request->file('img')->store('img', 'public');
        }
        $image->save();

        return redirect()->route('images.index');
    }


    /**
     * @param Image $image
     * @return Application|Factory|View
     */
    public function show(Image $image)
    {
        $comments = $image->comments;
        if ($comments->count() > 0) {
            $avg = round($comments->sum('rating') / $comments->count(), 1);
        } else {
            $avg = 0;
        }
        return view('client.images.show', compact('image', 'avg', 'comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Application|Factory|View
     */
    public function edit(Image $image)
    {
        return view('client.images.edit', compact('image'));
    }


    /**
     * @param Request $request
     * @param Image $image
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, Image $image): RedirectResponse
    {
        $this->authorize('update', $image);

        $request->validate([
            'user_id' => ['required'],
            'img' => ['required', 'mimes:jpg,png'],
            'name' => ['required', 'max:192', 'min:2', 'string'],
        ]);

        $old_img_path = null;
        $data = $request->all();
        if ($request->hasFile('img')) {
            $path = $request->file('img')->store('img', 'public');
            $data['img'] = $path;

            $old_img_path = $image->img;
        }
        if($old_img_path) {
            Storage::delete("public/" . $old_img_path);
        }

        $image->update($data);

        return redirect()->route('images.show', ['image' => $image]);
    }


    /**
     * @param Image $image
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(Image $image): RedirectResponse
    {
        $this->authorize('delete', $image);

        if($image->img) {
            Storage::delete("public/" . $image->img);
        }
        $image->delete();

        return redirect()->route('images.index');
    }
}
