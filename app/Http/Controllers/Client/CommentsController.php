<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CommentsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'body' => 'bail|required|max:292|min:2|string',
            'rating' => 'bail|required|digits_between:1,5',
            'user_id' => 'required',
            'image_id' => 'required',
        ]);

        $comment = new Comment();
        $comment->body = $request->get('body');
        $comment->rating = $request->get('rating');
        $comment->user_id = $request->get('user_id');
        $comment->image_id = $request->get('image_id');

        $comment->save();

        return redirect()->route('images.show', ['image' => $comment->image]);
    }

    /**
     * @param Comment $comment
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(Comment $comment): RedirectResponse
    {
        $this->authorize('delete', $comment);
        $comment->delete();
        return redirect()->back();
    }
}
