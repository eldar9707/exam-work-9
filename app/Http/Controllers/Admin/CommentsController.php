<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Image;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $comments = Comment::orderBy('id', 'desc')->paginate(8);
        return view('admin.comments.index', compact('comments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $users = User::all();
        $images = Image::all();
        return view('admin.comments.create', compact('users', 'images'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'body' => 'bail|required|max:292|min:2|string',
            'rating' => 'bail|required|digits_between:1,5',
            'user_id' => 'required',
            'image_id' => 'required',
        ]);

        $comment = new Comment();
        $comment->body = $request->get('body');
        $comment->rating = $request->get('rating');
        $comment->user_id = $request->get('user_id');
        $comment->image_id = $request->get('image_id');
        $comment->save();

        return redirect()->route('admin.images.index');
    }

    /**
     * @param Comment $comment
     * @return Application|Factory|View
     */
    public function show(Comment $comment)
    {
        return view('admin.comments.show', compact('comment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Comment $comment
     * @return Application|Factory|View
     */
    public function edit(Comment $comment)
    {
        $users = User::all();
        $images = Image::all();
        return view('admin.comments.edit', compact('comment', 'users', 'images'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function update(Request $request, Comment $comment): RedirectResponse
    {
        $request->validate([
            'body' => 'bail|required|max:292|min:2|string',
            'rating' => 'bail|required|digits_between:1,5',
            'user_id' => 'required',
            'image_id' => 'required',
        ]);
        $data = $request->all();
        $comment->update($data);
        $comment->save();

        return redirect()->route('admin.comments.index');
    }

    /**
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function destroy(Comment $comment): RedirectResponse
    {
        $comment->delete();
        return redirect()->back();
    }
}
