@extends('layouts.app')

@section('content')
    <h3><b>Editing new Image</b></h3>

    <form enctype="multipart/form-data" class=" row g-3" method="POST" action="{{route('images.update', ['image' => $image])}}">
        @csrf
        @method('PUT')
        <div class="col-md-6">
            <label for="name"><b>Image name</b></label>
            <input type="text" value="{{ old('name') }}"
                   class="form-control  {{ $errors->has('name') == true ? 'is-invalid' : null }} "
                   id="name" name="name" placeholder="Image Name">
            @error('name')
            <div class="text-danger"> {{ $message }} </div>
            @enderror

            <div class="my-3">
                <img src="{{asset('/storage/' . $image->img)}}" alt="{{$image->img}}" style="width:50px;height:50px;">
                <input class="form-control {{ $errors->has('img') == true ? 'is-invalid' : null }} "
                       type="file" id="formFile" name="img" value="{{$image->img}}">
                @error('img')
                <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>

            <input type="hidden" name="user_id" value="{{Auth::id()}}">

            <div class="mb-5">
                <button type="submit" class="btn btn-secondary">Add</button>
                <a href=" {{ route('images.index') }} " class="btn btn-secondary">Back</a>
            </div>
        </div>
    </form>

@endsection
