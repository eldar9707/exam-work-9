@extends('layouts.app')
@section('content')

    <a href="{{ route('images.create') }}" class="btn btn-outline-success mb-4">Add New Image</a>
    <div class="row row-cols-1 row-cols-md-3">
        @foreach($images as $image)
            <div class="col mb-4">
                <div class="card h-100">
                    <div class="card-body">
                        <a href="{{route('images.show', ['image' => $image])}}">
                            <img src="{{asset('/storage/' . $image->img)}}" alt="{{$image->img}}" class="card-img-top mb-2">
                        </a>
                        <h5 class="card-title">{{$image->name}}</h5>
                        <small class="text-muted">By:
                            <a href="{{route('users.show', ['user' => $image->user])}}" class="text-decoration-none">{{$image->user->name}}</a>
                        </small>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            {{ $images->links('pagination::bootstrap-4') }}
        </div>
    </div>

@endsection
