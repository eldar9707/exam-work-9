@extends('layouts.app')

@section('content')
    @if(Auth::user() == $user)
        <div class="btn-group">
            <h3><b>{{$user->name}}, profile</b></h3>
            <a href="{{ route('users.edit', ['user' => Auth::user()]) }}" class="btn btn-outline-info ml-4">Edit Name</a>
            <a href="{{ route('images.create') }}" class="btn btn-outline-success">Add New Image</a>
        </div>
        <div class="my-3">
            <h4>My Photos:</h4>
        </div>
        @else
        <h3><b>{{$user->name}}, profile</b></h3>
    @endif

    <table class="table mt-3" style="padding-top: 30px">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Image</th>
            <th scope="col" class="text-center">Name</th>
            @if(Auth::user() == $user)
                <th scope="col" class="text-right">Action</th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($user->images as $image)
            <tr>
                <td>
                    <a href="{{route('images.show', ['image' => $image])}}" class="text-decoration-none text-dark">
                        <img src="{{asset('/storage/' . $image->img)}}" alt="{{asset('/storage/' . $image->img)}}"
                             width="70px" height="70px">
                    </a>
                </td>
                <td class="text-center">
                    <a href="{{route('images.show', ['image' => $image])}}" class="text-decoration-none text-dark">
                        {{$image->name}}
                    </a>
                </td>

                <td class="text-right">
                    @can('delete', $image)
                        <div class="btn-group">
                            <form class="my-3" action="{{ route('images.destroy', ['image' => $image]) }}"
                                  method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-outline-danger btn-sm">Delete Image</button>
                                <a href="{{route('images.edit', ['image' => $image])}}"
                                   class="btn btn-outline-info btn-sm">Edit Image</a>
                            </form>
                        </div>
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>




    <div>
        <a href=" {{ route('images.index') }} " class="btn btn-outline-secondary">Back</a>
    </div>
@endsection
