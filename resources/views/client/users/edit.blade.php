@extends('layouts.app')

@section('content')
    <h3><b>Edit User name</b></h3>

    <form class=" row g-3" method="POST" action="{{route('users.update', ['user' => $user])}}">
        @csrf
        @method('PUT')
        <div class="col-md-6">
            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                           value="{{ $user->name }}" required autocomplete="name" autofocus>

                    @error('name')
                    <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <input type="hidden" name="user_id" value="{{Auth::id()}}">

            <div class="mb-5">
                <button type="submit" class="btn btn-secondary">Update Name</button>
                <a href=" {{ route('images.index') }} " class="btn btn-secondary">Back</a>
            </div>
        </div>
    </form>

@endsection
