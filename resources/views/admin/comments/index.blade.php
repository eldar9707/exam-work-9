@extends('layouts.app')
@section('content')

    <a href="{{route('admin.comments.create')}}" class="btn btn-outline-success">Create Comment</a>
    <table class="table mt-3" style="padding-top: 30px">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Comment Body</th>
            <th scope="col" class="text-center">User Name</th>
            <th scope="col" class="text-center">Image</th>
            <th scope="col" class="text-right">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($comments as $comment)
            <tr>
                <td>
                    <a href="{{route('admin.comments.show', ['comment' => $comment])}}">{{$comment->body}}</a>
                </td>
                <td class="text-center">
                    <a href="{{route('admin.users.show', ['user' => $comment->user])}}">{{$comment->user->name}}</a>
                </td>

                <td class="text-center">
                    <a href="{{route('admin.images.show', ['image' => $comment->image])}}">{{$comment->image->name}}</a>
                </td>

                <td class="text-right">
                    <div class="btn-group">
                        <form class="my-3" action="{{ route('admin.comments.destroy', ['comment' => $comment]) }}"
                              method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-outline-danger btn-sm">Delete Comment</button>
                            <a href="{{route('admin.comments.edit', ['comment' => $comment])}}"
                               class="btn btn-outline-info btn-sm">Edit Comment</a>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            {{ $comments->links('pagination::bootstrap-4') }}
        </div>
    </div>

@endsection
