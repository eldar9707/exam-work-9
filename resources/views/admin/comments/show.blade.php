@extends('layouts.app')

@section('content')
    <div class="btn-group">
        <h3><b>Comment by {{$comment->user->name}}</b></h3>
        <a href="{{ route('admin.comments.edit', ['comment' => $comment]) }}" class="btn btn-outline-info ml-4">Edit Comment</a>
    </div>

    <div>
        <a href=" {{ route('admin.comments.index') }} " class="btn btn-outline-secondary">Back</a>
    </div>
@endsection
