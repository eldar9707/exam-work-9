@extends('layouts.app')
@section('content')

    <a href="{{route('admin.images.create')}}" class="btn btn-outline-success">Create Image</a>
    <table class="table mt-3" style="padding-top: 30px">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Image</th>
            <th scope="col" class="text-center">Name</th>
            <th scope="col" class="text-center">User Name</th>
            <th scope="col" class="text-right">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($images as $image)
            <tr>
                <td>
                    <a href="{{route('admin.images.show', ['image' => $image])}}" class="text-decoration-none text-dark">
                        <img src="{{asset('/storage/' . $image->img)}}" alt="{{asset('/storage/' . $image->img)}}"
                         width="70px" height="70px">
                    </a>
                </td>
                <td class="text-center">
                    <a href="{{route('admin.images.show', ['image' => $image])}}" class="text-decoration-none text-dark">
                        {{$image->name}}
                    </a>
                </td>

                <td class="text-center">
                    <a href="{{route('admin.users.show', ['user' => $image->user])}}" class="text-decoration-none text-dark">
                        {{$image->user->name}}
                    </a>
                </td>

                <td class="text-right">
                    <div class="btn-group">
                        <form class="my-3" action="{{ route('admin.images.destroy', ['image' => $image]) }}"
                              method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-outline-danger btn-sm">Delete Image</button>
                            <a href="{{route('admin.images.edit', ['image' => $image])}}"
                               class="btn btn-outline-info btn-sm">Edit Image</a>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            {{ $images->links('pagination::bootstrap-4') }}
        </div>
    </div>

@endsection
