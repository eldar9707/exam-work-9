@extends('layouts.app')

@section('content')
    <form class="my-3" action="{{ route('admin.images.destroy', ['image' => $image]) }}"
          method="POST">
        @method('DELETE')
        @csrf
        <button type="submit" class="btn btn-outline-danger btn-sm">Delete Image</button>
    </form>

    <h3><b>{{$image->name}}</b></h3>
    <div>
        <img src="{{asset('/storage/' . $image->img)}}" class="img-fluid my-3"
             style=" max-width: 100%;" alt="{{$image->img}}">
    </div>
    <div>
        <span>
        <b>Average score: </b>
            {{$avg}}
        </span>
    </div>

    <h3 class="mt-4"><b>Add Comment</b></h3>
    <form enctype="multipart/form-data" class=" row g-3" method="POST" action="{{route('admin.comments.store')}}">
        @csrf
        <div class="col-md-6">
            <label for="body"><b>Comment</b></label>

            <textarea name="body" class="form-control {{ $errors->has('body') == true ? 'is-invalid' : null }} "
                      placeholder="Leave a comment here" id="body"
                      style="height: 100px">{{ old('body') }}</textarea>
            @error('body')
            <div class="text-danger"> {{ $message }} </div>
            @enderror

            <div class="form-group col-md-4">
                <label for="rating">Score</label>
                <select id="rating" class="form-control {{ $errors->has('rating') == true ? 'is-invalid' : null }} "
                        name="rating">
                    <option selected>Choose...</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
                @error('rating')
                <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>

            <input type="hidden" name="user_id" value="{{Auth::id()}}">
            <input type="hidden" name="image_id" value="{{$image->id}}">

            <div class="mb-5">
                <button type="submit" class="btn btn-secondary">Add Comment</button>
            </div>
        </div>
    </form>

    <div class="list-group mt-3">
        @foreach($comments as $comment)
            <div  class="list-group-item list-group-item-action">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1"><b>By:</b> {{$comment->user->name}}, <b class="text-success">Score: {{$comment->rating}}</b></h5>
                    <form class="my-3" action="{{ route('admin.comments.destroy', ['comment' => $comment]) }}"
                          method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-outline-danger btn-sm">Delete Comment</button>
                    </form>
                </div>
                <p class="mb-1">{{$comment->body}}</p>
            </div>
        @endforeach
    </div>
@endsection
