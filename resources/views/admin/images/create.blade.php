@extends('layouts.app')

@section('content')
    <h3><b>Adding new Image</b></h3>

    <form enctype="multipart/form-data" class=" row g-3" method="POST" action="{{route('admin.images.store')}}">
        @csrf
        <div class="col-md-6">
            <label for="name"><b>Image name</b></label>
            <input type="text" value="{{ old('name') }}"
                   class="form-control  {{ $errors->has('name') == true ? 'is-invalid' : null }} "
                   id="name" name="name" placeholder="Image Name">
            @error('name')
            <div class="text-danger"> {{ $message }} </div>
            @enderror

            <div>
                <label for="formFile" class="form-label mt-3"><b>Image</b></label>
                <input class="form-control {{ $errors->has('img') == true ? 'is-invalid' : null }} "
                       type="file" id="formFile" name="img">
                @error('img')
                <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>

            <div>
                <label for="user_id"><b>User</b></label>
                <select id="user_id" class="form-control {{ $errors->has('user_id') == true ? 'is-invalid' : null }} "
                        name="user_id">
                    <option selected>Choose...</option>
                    @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
                @error('rating')
                <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="mt-3">
                <button type="submit" class="btn btn-secondary">Add</button>
                <a href=" {{ route('images.index') }} " class="btn btn-secondary">Back</a>
            </div>
        </div>
    </form>

@endsection
