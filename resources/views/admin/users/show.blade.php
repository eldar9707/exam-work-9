@extends('layouts.app')

@section('content')
    <div class="btn-group">
        <h3><b>{{$user->name}}, profile</b></h3>
        <a href="{{ route('admin.users.edit', ['user' => $user]) }}" class="btn btn-outline-info ml-4">Edit Name</a>
    </div>
    <div class="my-3">
        <h4>User Photos:</h4>
    </div>

    <table class="table mt-3" style="padding-top: 30px">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Image</th>
            <th scope="col" class="text-center">Name</th>
            <th scope="col" class="text-right">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($user->images as $image)
            <tr>
                <td>
                    <a href="{{route('admin.images.show', ['image' => $image])}}" class="text-decoration-none text-dark">
                        <img src="{{asset('/storage/' . $image->img)}}" alt="{{asset('/storage/' . $image->img)}}"
                             width="70px" height="70px">
                    </a>
                </td>
                <td class="text-center">
                    <a href="{{route('admin.images.show', ['image' => $image])}}" class="text-decoration-none text-dark">
                        {{$image->name}}
                    </a>
                </td>

                <td class="text-right">
                    <div class="btn-group">
                        <form class="my-3" action="{{ route('admin.images.destroy', ['image' => $image]) }}"
                              method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-outline-danger btn-sm">Delete Image</button>
                            <a href="{{route('admin.images.edit', ['image' => $image])}}"
                               class="btn btn-outline-info btn-sm">Edit Image</a>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>




    <div>
        <a href=" {{ route('admin.images.index') }} " class="btn btn-outline-secondary">Back</a>
    </div>
@endsection
