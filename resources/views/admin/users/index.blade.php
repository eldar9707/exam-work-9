@extends('layouts.app')
@section('content')

    <a href="{{route('admin.users.create')}}" class="btn btn-outline-success">Create User</a>
    <table class="table mt-3" style="padding-top: 30px">
        <thead class="thead-dark">
        <tr>
            <th scope="col">User Name</th>
            <th scope="col" class="text-center">Email</th>
            <th scope="col" class="text-center">Is Admin</th>
            <th scope="col" class="text-right">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>
                    <a href="{{route('admin.users.show', ['user' => $user])}}">{{$user->name}}</a>
                </td>
                <td class="text-center">
                    {{$user->email}}
                </td>

                <td class="text-center">
                    @if($user->is_admin)
                        Admin
                    @else
                        User
                    @endif
                </td>

                <td class="text-right">
                    <div class="btn-group">
                        <form class="my-3" action="{{ route('admin.users.destroy', ['user' => $user]) }}"
                              method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-outline-danger btn-sm">Delete User</button>
                            <a href="{{route('admin.users.edit', ['user' => $user])}}"
                               class="btn btn-outline-info btn-sm">Edit User</a>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            {{ $users->links('pagination::bootstrap-4') }}
        </div>
    </div>

@endsection
