<?php

namespace Database\Factories;

use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\Factory;

class ImageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Image::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $path = $this->faker->image(
            storage_path() . "/app/public/img",'500', '500',
        );
        $db_path = explode('/public/', $path);

        return [
            'name' => $this->faker->realText(rand(10, 15)),
            'img' => end($db_path),
        ];
    }
}
