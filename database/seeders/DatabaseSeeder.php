<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Image;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(5)->create();
        $users = User::all();
        $users->first()->update([
            'email' => 'admin@admin.com',
            'is_admin' => true
        ]);

        for ($i = 0; $i < 15; $i++) {
            Image::factory()->state([
                'user_id' => $users->random()
            ])->create();
        }

        $images = Image::all();

        for ($i = 0; $i < 40; $i++) {
            Comment::factory()->state([
                'user_id' => $users->random(),
                'image_id' => $images->random(),
            ])->create();
        }
    }
}
