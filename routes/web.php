<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\Client\ImagesController::class, 'index'])
    ->name('images.index');

Route::resource('users', \App\Http\Controllers\Client\UsersController::class)
    ->only('show', 'edit', 'update')
    ->middleware('auth');

Route::resource('images', \App\Http\Controllers\Client\ImagesController::class)
    ->except('index', 'create', 'store');

Route::get('imagas/create', [\App\Http\Controllers\Client\ImagesController::class, 'create'])
    ->name('images.create')->middleware('auth');

Route::post('images', [\App\Http\Controllers\Client\ImagesController::class, 'store'])
    ->name('images.store')->middleware('auth');

Route::resource('comments', \App\Http\Controllers\Client\CommentsController::class)
    ->only('store', 'destroy')
    ->middleware('auth');


Route::prefix('admin')->middleware(['auth', 'admin'])->name('admin.')->group(function () {
    Route::resources([
        'images' => \App\Http\Controllers\Admin\ImagesController::class,
        'comments' => \App\Http\Controllers\Admin\CommentsController::class,
        'users' => \App\Http\Controllers\Admin\UsersController::class
    ]);
});

Auth::routes();
